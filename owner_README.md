## openKylin i18n SIG 组 各项目Owner须知

### 概述
本文档用于帮助各组件 owner 参与到 openKylin 多语言模块相关工作，帮助社区用户了解 openKylin 多语言机制等。

### openKylin 多语言实现机制
首先简单了解 locales 包，locales 包用于提供本地化（Localization）和国际化（Internationalization）的支持。它包含了用于翻译和本地化应用程序界面的语言和区域设置信息。系统中的 /etc/locale.gen 文件由该包提供。
/etc/locale.gen 文件用于配置系统支持的语言环境设置，包括启用和禁用不同的语言和区域设置。  
  
openKylin 系统安装时会对该文件进行修改，补充对应多语言字段；而在安装完成系统后，如果需要安装新的语言包，也会对该文件进行修改，同样的，也是补充对应多语言字段。  
  
在设置完语言环境后，系统会去固定目录中寻找组件、应用的翻译文件。首先会去 /usr/share/locale 寻找，如果应用程序找到了匹配用户首选语言的翻译文件，它将首先使用这些文件；如果在 /usr/share/locale 中没有找到匹配的翻译文件，应用程序将继续搜索 /usr/share/locale-langpack 目录；如果找不到匹配的翻译文件，应用程序将继续回退并尝试默认的语言文件。  
  
在 openKylin 中，目前实现多语言首先是通过系统安装增加系统可支持语言环境变量以及默认安装系统语言，也就是修改 /etc/locale.gen 文件；控制面板会读取 /etc/locale.gen 并进行已有语言切换，无法进行安装，控制面板不会去修改 /etc/locale.gen 文件；新增语言都带有一个对应语言包来修改 /etc/locale.gen ，且这个语言包会带有部分基础组件多语言 ；最后通过组件/应用自带的翻译文件以及多语言软件包提供的翻译文件，支持系统多语言；  
  
### 如何参与
在开始前请先注册 [gitee](https://gitee.com) 和 [weblate](https://weblate.openkylin.top/) 账户。

#### gitee
gitee 平台的使用这里就不再赘述。各组件分别对应有 gitee 仓库，仓库中各分支对应不同版本，仓库中就存储了代码、翻译文件等内容。

#### weblate
openkylin中的 weblate 平台，地址 https://weblate.openkylin.top/projects/ ，weblate 是一个基于 Web、与版本控制紧密集成的翻译工具。它拥有简洁清爽的用户界面，跨部件翻译同步、质量检查以及自动链接到源文件等功能。  

#### 工作流程

![工作流程图](./img/2024-05-24_16-29-13.png)

1. 首先确认各组件与 weblate 子部件是否完成自动化绑定。请确认各组件以及各组件代码仓库中的翻译文件，以及在 weblate 上是否存在对应子部件。[i18n-management](https://gitee.com/openkylin/i18n-management/) 项目中存在一个 [i18n.yaml](https://gitee.com/openkylin/i18n-management/blob/master/i18n.yaml) 文件，该文件会记录当前已存在 weblate 中子部件对应 openKylin 中组件项目仓库信息。以peony为例：

```
- name:
  - peony
  - owner: larue
  - branch: openkylin/nile
    - weblate-project: peony-qt
    - files: translations/peony-qt/peony-qt_*.ts
    - weblate-project: peony-qt-desktop
    - files: translations/peony-qt-desktop/peony-qt-desktop_*.ts
    - weblate-project: libpeony-qt
    - files: translations/libpeony-qt/libpeony-qt_*.ts
```

- 文件中 `package` 字段之后每 `- name` 字段下的内容表示对应 openkylin 仓库名称
- 文件中 `- name` 字段之后 `owner` 字段表示该项目对应 weblate 中的负责人
- 文件中 `- branch` 字段表示当前 `- name` 中内容的项目对应的分支
- 文件中 `- weblate-project` 字段表示当前 `- name` 中内容的项目对应的 weblate 部件名称
- 文件中 `- files` 字段表示当前 `weblate-project` 中内容的部件对应的翻译文件地址

2. 当记录在 [i18n.yaml](https://gitee.com/openkylin/i18n-management/blob/master/i18n.yaml) 文件中的内容缺少了相关项目或缺少了翻译文件，请 fork [本仓库](https://gitee.com/openkylin/i18n-management/)并提交 pr 在 [i18n.yaml](https://gitee.com/openkylin/i18n-management/blob/master/i18n.yaml) 文件尾部或相关项目 `- name`下增加 `- weblate-project` 以及对应的 `- files`,请注意避免重名。

![输入图片说明](./img/2024-05-25_17-10-45.png)
![输入图片说明](./img/2024-05-25_17-12-41.png)

3. 当记录在 [i18n.yaml](https://gitee.com/openkylin/i18n-management/blob/master/i18n.yaml) 文件中的 `- name` 对应项目中，对应 `- branch` 分支里的文件 `- files` 被社区开发者上传或更新时，翻译就会自动同步到 weblate 对应部件`- webalte-project`中。同时在 weblate 上进行翻译添加、更改、校验等操作后，也会定期自动同步到该文件对应 openKylin 仓库中。

4. 当所有翻译文件更新完毕，即可提升版本号，将带有最新翻译文件的包同步进软件源。

#### weblate使用

当项目开发者在完成对应的翻译文件上传并且进行了正确的设置后，weblate 会有如下的项目界面显示:   
  
![weblate界面示例](https://gitee.com/kylinos-i18n/language-packs/raw/master/img/3.png)  

其次请确认自己是否有对应项目 weblate 中的权限，如果无法对对应 weblate 项目提交修改，请向 [i18n-management](https://gitee.com/openkylin/i18n-management) 提交 i18n SIG 相关 issue ：  

以 ukui-clock 为例，在这里初步的语言种类是基于自己所传的各翻译文件的情况，以下为添加新语种翻译并自动翻译的步骤：
1.  点击下方的开始新翻译
2.  在搜索框中输入待添加语种
3.  点击开始新翻译等待加载，到达如下界面；如果已存在需要处理的语种，直接点击进行翻译处理即可；  

![添加新语种翻译](https://gitee.com/kylinos-i18n/language-packs/raw/master/img/4.png) 

4.  weblate 项目中会存在一些带有 -template 后缀的子部件，这些部件定义为翻译模板，模板中的翻译文件内容都是经过翻译团队校验确认的内容，该部件用于提供给其他部件翻译模板，模板文件不需要研发、用户处理，该部件由翻译团队处理。  
在待翻译的子部件 `- weblate-project` 中，点击如上界面后点击上方菜单的 工具->自动化翻译，使用 weblate 自动翻译时请尽量使用模板翻译文件进行翻译，同时将  **自动翻译模式** 选择为添加为翻译， **搜索筛选器** 选择未翻译字符串。如果存在使用了翻译模板翻译后还有无法翻译的内容，请建立相关issue指派给 @段凯文，同时请备注好需要更新的 `- weblate-project`,避免无法准确更新翻译文件 。

![使用模板进行自动翻译](./img/2024-05-24_17-18-07.png)
![添加新语种翻译2](https://gitee.com/kylinos-i18n/language-packs/raw/master/img/5.png) 

等待其完成会提示“自动翻译已完成，xxx 个字符串已更新。”,有时候api会出现繁忙情况，请稍作等待。

5. 在机器翻译完之后，平台会反馈一些翻译有明显问题的翻译显示，红色标记的属于机器已经翻译但是可能存在问题的翻译，这一部分就需要手动处理。同样，也会存在部分字段翻译不准确的情况，这些也需要手动处理。还有一些字符串，社区中用户会选择给出翻译建议的内容，这些也需要手动处理。点击‘所有字符串’可以逐个筛查错误翻译以及翻译建议，也可以用到上方的搜索来帮助你快速定位字符串。请注意每修改一个字段后保障 '修改编辑' 的未被选中，并请点击‘保存并继续’，不点击保存修改将会被清除。 

![字段检查示例](https://gitee.com/kylinos-i18n/language-packs/raw/master/img/6.png)
  
![字段检查示例](https://gitee.com/kylinos-i18n/language-packs/raw/master/img/7.png)

6. 在完成所有翻译检查后，点击 管理->代码库维护，点击提交后再点击推送，将最新的改动推送至 gitee 仓库。

![输入图片说明](./img/2024-05-25_11-48-05.png)

7. 最后回到各组件对应 gitee 仓库，即可看见在 weblate 中对翻译文件的修改。提升版本号即可将改动同步进软件源。

![输入图片说明](./img/2024-05-27_17-22-29.png)

通过 weblate 平台更新生成新的翻译，这就是一次对openkylin i18n SIG的贡献。以上就是 openKylin 多语言参与文档，欢迎各位社区小伙伴积极参与~

