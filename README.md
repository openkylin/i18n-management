# openKylin i18n SIG
## 介绍 

i18n-management负责openKylin中各项目多语言翻译的维护和持续化的贡献。该仓库记录了openKylin中各个需要维护翻译的组件以及对应在weblate平台中的信息。

在i18n SIG中，用户可找到感兴趣组件的翻译进行特定贡献，可通过一定的贡献量获得对应组件的管理权限。

## 原则

- i18n SIG所维护的平台翻译都是开放的，任何人和组织都可以参与。
- 在 i18n-management的i18n.yaml 文件中包含了该项目需要在weblate上建的项目名称、管理者、翻译文件地址。
- 每个项目在weblate翻译平台的项目核心成员都拥有此项目的维护者权限，共同维护好组件的翻译状况。

## 如何参与

接下来将逐步介绍OpenKylin用户如何在weblate翻译平台进行翻译贡献。
  
#### weblate平台账号创建
  
weblate翻译平台网址：[https://weblate.openkylin.top/projects/](https://weblate.openkylin.top/projects/) 进入网址进行账号注册成功后即成为了已验证用户。
先明确openkylin在weblate的用户权限分为五类：超级权限者、管理者、各项目维护者、已验证的用户、游客。i18n SIG组的owner属于超级权限者，i18N其他成员拥有管理者权限，其他sig的owner和maintainer具有其对应项目的维护者权限，已验证的用户具备对项目翻译进行建议的权限，游客仅可查看翻译。

#### 职责划分：

超级权限者主要负责以下任务:
1. 负责分发各项目维护者权限给到各需要维护weblate翻译的sig组maintainer上。
2. 负责在weblate上建对应sig组的项目。

管理者主要负责以下任务:
1. 管理者基本具备所有权利，可帮忙负责权限分发和处理已验证用户所提的翻译建议等。
2. 审核其他sig组提出的翻译组件创建申请，并帮忙在weblate中建好对应的项目。

项目维护者主要负责以下任务：
1. 负责在对应的项目中添加并维护翻译组件
2. 分发组内权限给与其他本sig内的成员
3. 审核处理用户提出的翻译建议

#### 职责划分以及如何贡献

##### 游客和验证用户：

###### weblate 子部件中已有语言  
  
1. 以测试项目 kylin-os-installer 为例，weblate进入如下界面后在字符串一栏进行选择，首先可以选择“未完成的字符串”，这一块基本上是属于没有翻译的部分，用户可以对此模块进行建议和查看。

![1.png](./img/1.png)

2. 点击“未翻译字符串”进入以下界面，会出现对应语言翻译内容为空的情况，这时如果有确认的翻译可以填在空白处，然后点击建议按钮，进行建议保存。注意下完成一个字符串的翻译建议后，一定要点击建议，否则会出现建议未被保存的情况 

![2.png](./img/2024-06-11_11-19-20.png)
  
3. 在完成一次项目的翻译建议后，在 openKylin gitee 中对应项目中建立相关 issue。项目的维护者会根据issue内容，验证相关翻译，如果无误会接受你的建议。  

![3.png](img/c9f06249-91e1-4d0a-8389-ef5aecc11ba8.png)
  
######  weblate 子部件中缺少语言

1. 还是以测试项目 kylin-os-installer 为例，进入该子部件后，如果缺少需要补充翻译的语言，请点击子部件最下方“开始新翻译”，并选出需要增加的语言，之后重复 [weblate 子部件已有语言](https://gitee.com/openkylin/i18n-management/tree/master#weblate-%E5%AD%90%E9%83%A8%E4%BB%B6%E4%B8%AD%E5%B7%B2%E6%9C%89%E8%AF%AD%E8%A8%80) 步骤，进行翻译建议

![4.png](img/2024-06-11_11-29-29.png)  

##### 项目维护者：
1. 如果你是一个项目的维护者或管理者，这意味着需要对整个项目的数据和维护负责，这需要每个拥有项目管理权限的人都要熟悉weblate的项目管理和基本的操作流程，https://docs.weblate.org/zh_CN/latest/user/basic.html 是weblate官方文档地址。
2. 需要熟悉[openKylin i18n SIG 组 各项目Owner须知](./owner_README.md)。

## 其他注意事项

- weblate平台地址：[https://weblate.openkylin.top/projects/](https://weblate.openkylin.top/projects/)
